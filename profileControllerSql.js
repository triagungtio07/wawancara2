const db = require("./models");
const jwt = require("jsonwebtoken");
const secretKey = process.env.SECRET_KEY;

module.exports = {
  get: async (req, res) => {
    try {
      const authenticate = req.headers.authorization;
      const verified = jwt.verify(authenticate, secretKey);

      if (!verified) {
        throw new Error("Access Denied");
      }

      const data = await db.Profiles.findAll();

      res.json({ message: "succes get profile data", data: data });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  post: async (req, res) => {
    try {
      const payload = req.body;
      const data = await db.Profiles.create(payload);
      const token = jwt.sign(
        {
          name: data.name,
        },
        secretKey
      );
      res.json({
        message: "succes insert profile data",
        data: { data: data, token: token },
      });
    } catch (error) {}
  },
  put: async (req, res) => {
    try {
      const age = req.body["age"];
      const address = req.body["address"];

      const { id } = req.params;
      const authenticate = req.headers.authorization;
      const verified = jwt.verify(authenticate, secretKey);

      if (!verified) {
        throw new Error("Access Denied");
      }
      const data = await db.Profiles.update(
        {
          age: age,
          address: address,
        },
        { where: { id: id } }
      );
      res.json({ message: "succes update profile data", data: data });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  delete: async (req, res) => {
    try {
      const { id } = req.params;
      const authenticate = req.headers.authorization;
      const verified = jwt.verify(authenticate, secretKey);

      if (!verified) {
        throw new Error("Access Denied");
      }
      const data = await db.Profiles.destroy({ where: { id: id } });
      res.json({ message: "succes delete profile data", data: data });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
};
