const express = require("express");
const app = express();
const userController = require("./userControllerMongo");
const userController2 = require("./userControllerSql");
const profileController = require("./profileControllerMongo");
const profileController2 = require("./profileControllerSql");
require("dotenv").config();

const port = process.env.PORT || 3500;

app.use(express.json());

//Test Heroku
app.get("/", (req, res) => {
  res.send("<h1>Hai Teman-Teman</h1>");
});

//Users Mongodb
app.get("/users", userController.get);
app.post("/users", userController.post);
app.put("/users/:_id", userController.put);
app.delete("/users/:_id", userController.delete);

//Users SQL
app.get("/users/sql", userController2.get);
app.post("/users/sql", userController2.post);
app.put("/users/sql/:id", userController2.put);
app.delete("/users/sql/:id", userController2.delete);

//Profile Mongodb
app.get("/profile", profileController.get);
app.post("/profile", profileController.post);
app.put("/profile/:_id", profileController.put);
app.delete("/profile/:_id", profileController.delete);

//profile SQL
app.get("/profile/sql", profileController2.get);
app.post("/profile/sql", profileController2.post);
app.put("/profile/sql/:id", profileController2.put);
app.delete("/profile/sql/:id", profileController2.delete);

app.listen(port, console.log("running server.."));
