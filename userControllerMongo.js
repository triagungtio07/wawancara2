const userModel = require("./mongoModel/users")();
const jwt = require("jsonwebtoken");
const secretKey = process.env.SECRET_KEY;

module.exports = {
  get: async (req, res) => {
    try {
      const authenticate = req.headers.authorization;
      const verified = jwt.verify(authenticate, secretKey);

      if (!verified) {
        throw new Error("Access Denied");
      }
      const data = await userModel.find().populate("profile");

      res.json({ message: "succes get user data", data: data });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  post: async (req, res) => {
    try {
      const payload = req.body;
      const data = await userModel.create(payload);
      const token = jwt.sign(
        {
          name: data.name,
        },
        secretKey
      );

      res.json({
        message: "succes insert user data",
        data: { data: data, token: token },
      });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  put: async (req, res) => {
    try {
      const payload = req.body;
      const id = req.params;
      const authenticate = req.headers.authorization;
      const verified = jwt.verify(authenticate, secretKey);

      if (!verified) {
        throw new Error("Access Denied");
      }
      const data = await userModel.findByIdAndUpdate(id, payload);
      res.json({ message: "succes update user data", data: data });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  delete: async (req, res) => {
    try {
      const id = req.params;
      const authenticate = req.headers.authorization;
      const verified = jwt.verify(authenticate, secretKey);

      if (!verified) {
        throw new Error("Access Denied");
      }
      const data = await userModel.findByIdAndDelete(id);
      res.json({ message: "succes delete user data", data: data });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
};
