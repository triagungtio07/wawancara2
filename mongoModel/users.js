const mongoose = require("mongoose");
const Profile = require("./profile");
require("dotenv").config();

mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const usersSchema = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
  profile: {
    type: mongoose.Schema.Types.ObjectId,
    ref: Profile,
  },
});

module.exports = function UsersModel() {
  return mongoose.model("users", usersSchema);
};
