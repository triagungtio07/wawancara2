const mongoose = require("mongoose");
require("dotenv").config();

mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const profileSchema = new mongoose.Schema({
  age: Number,
  address: String,
});

module.exports = function ProfileModel() {
  return mongoose.model("profile", profileSchema);
};
