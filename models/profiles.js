"use strict";
const { Model } = require("sequelize");
const profile = require("../mongoModel/profile");
module.exports = (sequelize, DataTypes) => {
  class Profiles extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Profiles.belongsTo(models.Users, {
        foreignKey: "users_id",
        as: "users",
      });
    }
  }
  Profiles.init(
    {
      age: DataTypes.INTEGER,
      address: DataTypes.STRING,
      users_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Profiles",
    }
  );
  return Profiles;
};
