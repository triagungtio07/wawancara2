const db = require("./models");
const jwt = require("jsonwebtoken");
const secretKey = process.env.SECRET_KEY;

module.exports = {
  get: async (req, res) => {
    try {
      const authenticate = req.headers.authorization;
      const verified = jwt.verify(authenticate, secretKey);

      if (!verified) {
        throw new Error("Access Denied");
      }

      const data = await db.Users.findAll({
        include: [
          {
            model: db.Profiles,
            as: "Profiles",
          },
        ],
      });

      res.json({ message: "succes get user data", data: data });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  post: async (req, res) => {
    try {
      const { name, email, password, age, address } = req.body;
      const data = await db.Users.create(
        {
          name,
          email,
          password,
          Profiles: {
            age,
            address,
          },
        },
        {
          include: {
            model: db.Profiles,
            as: "Profiles",
          },
        }
      );
      const token = jwt.sign(
        {
          name: data.name,
        },
        secretKey
      );
      res.json({
        message: "succes insert user data",
        data: { data: data, token: token },
      });
    } catch (error) {}
  },
  put: async (req, res) => {
    try {
      const name = req.body["name"];
      const email = req.body["email"];
      const password = req.body["password"];
      const { id } = req.params;
      const authenticate = req.headers.authorization;
      const verified = jwt.verify(authenticate, secretKey);

      if (!verified) {
        throw new Error("Access Denied");
      }
      const data = await db.Users.update(
        {
          name: name,
          email: email,
          password: password,
        },
        { where: { id: id } }
      );
      res.json({ message: "succes update user data", data: data });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
  delete: async (req, res) => {
    try {
      const { id } = req.params;
      const authenticate = req.headers.authorization;
      const verified = jwt.verify(authenticate, secretKey);
      console.log(id);

      if (!verified) {
        throw new Error("Access Denied");
      }
      const data = await db.Users.destroy({ where: { id: id } });
      res.json({ message: "succes delete user data", data: data });
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  },
};
